package clickpvp;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Duell {

    public static String prefix = ChatColor.BLUE + "[LegendPvp] " + ChatColor.RESET;

    public static int delayInt = 0;

    public static HashMap<Integer, Player> delayTimerpPlayer = new HashMap<>();
    public static HashMap<Integer, Player> delayTimetpPlayer = new HashMap<>();
    public static HashMap<Integer, Integer> delayTimeLong = new HashMap<>();
    public static HashMap<Player, Integer> delayGetInt = new HashMap<>();

    public static int matchInt = 0;

    public static HashMap<Integer, Integer> matchTime = new HashMap<>();
    public static HashMap<Integer, ArrayList> matchPlayers = new HashMap<>();
    public static HashMap<Player, Integer> matchGetInt = new HashMap<>();
    public static HashMap<Integer, Integer> matchTask = new HashMap<>();
    public static HashMap<Integer, Boolean> matchShowTime = new HashMap<>();

    public static int moveInt = 0;

    public static HashMap<Integer, Integer> moveRemainingTime = new HashMap<>();
    public static HashMap<Integer, ArrayList> movePlayer = new HashMap<>();
    public static HashMap<Player, Integer> moveGetInt = new HashMap<>();

    public static void startDuell(final Player p1, final Player p2, int id) {
        delayTimerpPlayer.remove(id);
        delayTimetpPlayer.remove(id);
        delayTimeLong.remove(id);
        delayGetInt.remove(p2);
        
        if(delayGetInt.containsKey(p1)){
            int secondKey = delayGetInt.get(p1);
            delayTimeLong.remove(secondKey);
            delayTimerpPlayer.remove(secondKey);
            delayTimetpPlayer.remove(secondKey);
            delayGetInt.remove(p1);
        }
        
        
        matchTime.put(matchInt, 180);
        matchPlayers.put(matchInt, new ArrayList());
        matchPlayers.get(matchInt).add(p1);
        matchPlayers.get(matchInt).add(p2);
        matchGetInt.put(p1, matchInt);
        matchGetInt.put(p2, matchInt);
        matchShowTime.put(matchInt, true);

        p1.sendMessage(Duell.prefix + ChatColor.GREEN + "Das Duell gegen " + p2.getName() + " startet nun!");
        p2.sendMessage(Duell.prefix + ChatColor.GREEN + "Das Duell gegen " + p1.getName() + " startet nun!");
        if (p1.getLocation().distance(p2.getLocation()) > 10) {
            p1.teleport(p2);
        }

        final int matchIntForScheuduler = matchInt;
        matchTask.put(matchInt, Bukkit.getScheduler().scheduleSyncRepeatingTask(ClickPvp.getInstance(), new Runnable() {

            @Override
            public void run() {
                matchTime.put(matchIntForScheuduler, (matchTime.get(matchIntForScheuduler) - 1));
                if (matchTime.get(matchIntForScheuduler) == 0) {
                    endDuell(p1, p2, matchIntForScheuduler, winType.UNDECIDED);
                } else {
                    if (matchShowTime.get(matchIntForScheuduler)) {
                        ArrayList playersAL = matchPlayers.get(matchIntForScheuduler);
                        Player[] players = new Player[playersAL.size()];
                        players = (Player[]) playersAL.toArray(players);
                        for (Player p : players) {
                            ActionBar.sendActionBar(p, "Das Duell dauert noch " + matchTime.get(matchIntForScheuduler) + " Sekunden an!");
                        }
                    }
                }

            }

        }, 0, 1 * 20L));
        matchInt++;
    }

    public enum winType {
        WIN,
        UNDECIDED,
        DISCONNECT;
    }

    static PotionEffectType[] negativePotionEffects = new PotionEffectType[]{
        PotionEffectType.WITHER,
        PotionEffectType.CONFUSION,
        PotionEffectType.SLOW_DIGGING,
        PotionEffectType.HUNGER,
        PotionEffectType.POISON,
        PotionEffectType.SLOW,
        PotionEffectType.WEAKNESS,
        PotionEffectType.BLINDNESS
    };

    static PotionEffect heal = new PotionEffect(PotionEffectType.REGENERATION, 20 * 5, 3);

    public static void endDuell(Player winner, Player losser, int matchId, winType wt) {
        if (wt == winType.WIN || wt == winType.DISCONNECT) {
            Bukkit.broadcastMessage(prefix + ChatColor.GOLD + winner.getName() + ChatColor.GREEN + " hat das Duell gegen " + ChatColor.GOLD + losser.getName() + ChatColor.GREEN + " gewonnen!");
        } else if (wt == winType.UNDECIDED) {
            Bukkit.broadcastMessage(prefix + ChatColor.GREEN + "Das Duell zwischen " + ChatColor.GOLD + winner.getName() + ChatColor.GREEN + " und " + ChatColor.GOLD + losser.getName() + ChatColor.GREEN + " ging unendschienden aus!");
        }
        if (moveGetInt.containsKey(winner)) {
            int moveId = moveGetInt.get(winner);
            moveRemainingTime.remove(moveId);
            movePlayer.remove(moveId);
            moveGetInt.remove(winner);
            moveGetInt.remove(losser);
        }

        if (wt == winType.DISCONNECT) {
            for (PotionEffectType pet : negativePotionEffects) {
                winner.removePotionEffect(pet);
            }
            winner.addPotionEffect(heal);
            winner.setFireTicks(0);
        } else {

            for (PotionEffectType pet : negativePotionEffects) {
                winner.removePotionEffect(pet);
                losser.removePotionEffect(pet);
            }
            winner.addPotionEffect(heal);
            losser.addPotionEffect(heal);
            winner.setFireTicks(0);
            losser.setFireTicks(0);

        }
        matchTime.remove(matchId);
        matchPlayers.remove(matchId);
        matchGetInt.remove(winner);
        matchGetInt.remove(losser);
        matchShowTime.remove(matchId);
        Bukkit.getScheduler().cancelTask(matchTask.get(matchId));
    }

}
