package clickpvp.commands;

import clickpvp.Duell;
import clickpvp.events.DamageEvent;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class autoDeny implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if(!(cs instanceof Player)){
            cs.sendMessage("Du bist kein Spieler!");
            return true;
        }
        Player p = (Player) cs;
        if(DamageEvent.noRequest.contains(p)){
            p.sendMessage(Duell.prefix + ChatColor.GREEN + "AutoDeny wurde deaktiviert!");
            DamageEvent.noRequest.remove(p);
            return true;
        } else {
            p.sendMessage(Duell.prefix + ChatColor.GREEN + "AutoDeny wurde aktiviert!");
            DamageEvent.noRequest.add(p);
            return true;
        }
    }
    
}
