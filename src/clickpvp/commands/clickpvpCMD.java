package clickpvp.commands;

import clickpvp.Duell;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class clickpvpCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (!(cs instanceof Player)) {
            cs.sendMessage("Dieser Command ist nur Spieler benutzbar!");
            return true;
        }
        Player p = (Player) cs;
        if (args.length > 1) {
            int delayId = Integer.parseInt(args[1]);
            if (args[0].equalsIgnoreCase("accept")) {
                if (Duell.delayTimeLong.containsKey(delayId) && Duell.delayTimetpPlayer.get(delayId) == p) {
                    Player rp = Duell.delayTimerpPlayer.get(delayId);
                    Duell.startDuell(p, rp, delayId);
                    return true;
                } else {
                    p.sendMessage(Duell.prefix + ChatColor.RED + "Die Anfrage ist bereits abgelaufen!");
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("decline")) {
                if (Duell.delayTimeLong.containsKey(delayId) && Duell.delayTimetpPlayer.get(delayId) == p) {
                    Duell.delayTimerpPlayer.get(delayId).sendMessage(Duell.prefix + ChatColor.GOLD + p.getName() + ChatColor.GREEN + " hat die Duellanfrage abgelehnt!");
                    p.sendMessage(Duell.prefix + ChatColor.RED + "Du hast die Anfrage abgelehnt!");
                    Duell.delayTimeLong.remove(delayId);
                    Duell.delayTimerpPlayer.remove(delayId);
                    Duell.delayTimetpPlayer.remove(delayId);
                    return true;
                } else {
                    p.sendMessage(Duell.prefix + ChatColor.RED + "Die Anfrage ist bereits abgelaufen!");
                    return true;
                }
            } else {
                return false;
            }

        } else {
            return false;
        }
    }
}
