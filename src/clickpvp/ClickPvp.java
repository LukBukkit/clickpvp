package clickpvp;

import static clickpvp.Duell.endDuell;
import clickpvp.Duell.winType;
import clickpvp.events.DamageEvent;
import java.util.Iterator;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ClickPvp extends JavaPlugin {

    public static ClickPvp instance;

    @Override
    public void onEnable() {
        instance = this;

        this.getCommand("clickpvp").setExecutor(new clickpvp.commands.clickpvpCMD());
        this.getCommand("duell").setExecutor(new clickpvp.commands.autoDeny());

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new clickpvp.events.DamageEvent(), this);
        pm.registerEvents(new clickpvp.events.LeaveEvent(), this);
        pm.registerEvents(new clickpvp.events.MoveEvent(), this);
        pm.registerEvents(new clickpvp.events.PlayerJoin(), this);

        for(Player p : Bukkit.getOnlinePlayers()){
            if(p.hasPermission("Duell.YouTuber")){
                DamageEvent.noRequest.add(p);
            }
        }
        
        ActionBar.nmsver = Bukkit.getServer().getClass().getPackage().getName();
        ActionBar.nmsver = ActionBar.nmsver.substring(ActionBar.nmsver.lastIndexOf(".") + 1);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

            @Override
            public void run() {
                for (Map.Entry<Integer, Integer> entry : Duell.delayTimeLong.entrySet()) {
                    if ((entry.getValue() + 30) < ((int) (System.currentTimeMillis() / 1000))) {
                        int id = entry.getKey();
                        Player p1 = Duell.delayTimerpPlayer.get(id);
                        Player p2 = Duell.delayTimetpPlayer.get(id);
                        p1.sendMessage(Duell.prefix + ChatColor.RED + "Deine Anfrage an " + ChatColor.GOLD + p2.getName() + ChatColor.RED + " ist ausgelaufen!");
                        p2.sendMessage(Duell.prefix + ChatColor.RED + "Die Anfrage von "  + ChatColor.GOLD + p1.getName() + ChatColor.RED + " ist ausgelaufen!");
                        Duell.delayGetInt.remove(p1);
                        Duell.delayGetInt.remove(p2);
                        Duell.delayTimerpPlayer.remove(id);
                        Duell.delayTimetpPlayer.remove(id);
                        Duell.delayTimeLong.remove(id);
                    }
                }
            }

        }, 5L, 20L);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

            @Override
            public void run() {
                for (Map.Entry<Integer, Integer> entry : Duell.moveRemainingTime.entrySet()) {
                    int moveId = entry.getKey();
                    int time = entry.getValue();
                    time--;
                    Duell.moveRemainingTime.put(moveId, time);
                    Iterator it = Duell.movePlayer.get(moveId).iterator();
                    Player p1 = (Player) it.next();
                    Player p2 = (Player) it.next();
                    Duell.matchShowTime.put(Duell.matchGetInt.get(p1), false);
                    ActionBar.sendActionBar(p1, ChatColor.RED + "Ihr seid zuweit von einander entfernt. Bitte bewegt euch auf einander zu, sontst endet das Spiel in " + time + " Sekunden!");
                    ActionBar.sendActionBar(p2, ChatColor.RED + "Ihr seid zuweit von einander entfernt. Bitte bewegt euch auf einander zu, sontst endet das Spiel in " + time + " Sekunden!");
                    if (time <= 0) {
                        Duell.moveRemainingTime.remove(moveId);
                        Duell.movePlayer.remove(moveId);
                        Duell.moveGetInt.remove(p1);
                        Duell.moveGetInt.remove(p2);
                        endDuell(p1, p2, Duell.matchGetInt.get(p1), winType.UNDECIDED);
                    }
                }
            }
        }, 3L, 20L);
    }

    public static ClickPvp getInstance() {
        return instance;
    }

}
