package clickpvp.events;

import clickpvp.Duell;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class DamageEvent implements Listener {

    public Material[] swords = new Material[]{
        Material.WOOD_SWORD,
        Material.STONE_SWORD,
        Material.IRON_SWORD,
        Material.GOLD_SWORD,
        Material.DIAMOND_SWORD
    };

    public HashMap<Player, Integer> requestTime = new HashMap<>();
    
    public static ArrayList<Player> noRequest = new ArrayList<>();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamage(EntityDamageByEntityEvent e) {
        if (e.getEntityType().equals(EntityType.PLAYER) && Duell.matchGetInt.containsKey((Player) e.getEntity()) && e.getDamager().getType().equals(EntityType.PLAYER) && Duell.matchGetInt.containsKey((Player) e.getDamager())) {
            Player p = (Player) e.getEntity();
            Player dp = (Player) e.getDamager();
            int matchInt = Duell.matchGetInt.get(p);
            if ((p.getHealth() - e.getFinalDamage()) <= 2) {
                Duell.endDuell(dp, p, matchInt, Duell.winType.WIN);
                e.setCancelled(true);
            } else {
                e.setCancelled(false);
            }
        } else if (e.getEntityType().equals(EntityType.PLAYER) && e.getDamager().getType().equals(EntityType.PLAYER)) {
            Player tp = (Player) e.getEntity();
            Player rp = (Player) e.getDamager();

             if (Arrays.asList(swords).contains(rp.getItemInHand().getType()) && (!noRequest.contains(tp) || rp.hasPermission("Duell.YouTuber"))) {
                if (!(requestTime.containsKey(rp)) || requestTime.get(rp) + 30 < ((int) (System.currentTimeMillis() / 1000))) {
                    rp.sendMessage(Duell.prefix + ChatColor.GREEN + "Du hast " + ChatColor.GOLD + tp.getName() + ChatColor.GREEN + " zu einem Duell herausgefordert!");
                    TextComponent message = new TextComponent(Duell.prefix + ChatColor.GOLD + rp.getName() + ChatColor.GREEN + " hat dich zu einem Duell herausgefordert! Möchtest du " + ChatColor.GREEN + "annehemen?\n");

                    TextComponent extraYes = new TextComponent("Ja");
                    extraYes.setColor(net.md_5.bungee.api.ChatColor.GREEN);
                    extraYes.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/clickpvp accept " + Duell.delayInt));
                    extraYes.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Startet ein Duell mit " + rp.getName() + "!").create()));
                    message.addExtra(extraYes);

                    TextComponent space = new TextComponent(" / ");
                    message.addExtra(space);

                    TextComponent extraNo = new TextComponent("Nein");
                    extraNo.setColor(net.md_5.bungee.api.ChatColor.RED);
                    extraNo.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/clickpvp decline " + Duell.delayInt));
                    extraNo.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Lehnt die Anfrage von " + rp.getName() + " ab!").create()));
                    message.addExtra(extraNo);

                    tp.spigot().sendMessage(message);

                    Duell.delayTimeLong.put(Duell.delayInt, (int) (System.currentTimeMillis() / 1000));
                    Duell.delayTimerpPlayer.put(Duell.delayInt, rp);
                    Duell.delayTimetpPlayer.put(Duell.delayInt, tp);
                    Duell.delayGetInt.put(rp, Duell.delayInt);
                    Duell.delayGetInt.put(tp, Duell.delayInt);
                    Duell.delayInt++;

                    requestTime.put(rp, (int) (System.currentTimeMillis() / 1000));
                } else {
                    rp.sendMessage(Duell.prefix + ChatColor.RED + "Du musst 30 Sekunden bis zur nächsten Anfrage warten!");
                }
            }
            e.setCancelled(true);
        }
        if (e.getEntityType().equals(EntityType.PLAYER) && Duell.matchGetInt.containsKey((Player) e.getEntity())
                && e.getDamager().getType().equals(EntityType.ARROW) && (((Arrow) e.getDamager()).getShooter() instanceof Player) && Duell.matchGetInt.containsKey(((Player) ((Arrow) e.getDamager()).getShooter()))) {
            Player p = (Player) e.getEntity();
            Player dp = ((Player)((Arrow)e.getDamager()).getShooter());
            int matchInt = Duell.matchGetInt.get(p);
            if ((p.getHealth() - e.getFinalDamage()) <= 0) {
                Duell.endDuell(dp, p, matchInt, Duell.winType.WIN);
                e.setCancelled(true);
            } else {
                e.setCancelled(false);
            }
        } else if(e.getEntityType().equals(EntityType.PLAYER) && e.getDamager().getType().equals(EntityType.ARROW)){
            e.setCancelled(true);
        }
        if(e.getDamager() != null && e.getDamager().getType() != null && e.getDamager().getType().equals(EntityType.PLAYER) && 
                ((Player)e.getDamager()).getItemInHand() != null && ((Player)e.getDamager()).getItemInHand().getData() != null && 
                ((Player)e.getDamager()).getItemInHand().getItemMeta().getDisplayName() != null &&
                ((Player)e.getDamager()).getItemInHand().getItemMeta().getDisplayName().equals("§l§4Stern des Admins")){
            e.setCancelled(false);
        }
    }

}
