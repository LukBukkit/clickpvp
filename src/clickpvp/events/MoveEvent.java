package clickpvp.events;

import clickpvp.Duell;
import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveEvent implements Listener {

    HashMap<Integer, ArrayList> distanceBetween = new HashMap<>();
    HashMap<Player, Integer> getCountByPlayer = new HashMap<>();
    int counter = 0;

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (e.getFrom().getX() != e.getTo().getX() || e.getFrom().getY() != e.getTo().getY() || e.getFrom().getZ() != e.getTo().getZ()) {
            if (Duell.matchGetInt.containsKey(e.getPlayer())) {
                int matchId = Duell.matchGetInt.get(e.getPlayer());
                ArrayList al = (ArrayList) Duell.matchPlayers.get(matchId).clone();
                al.remove(e.getPlayer());
                Player mp = ((Player) al.iterator().next());
                if (mp.getLocation().distance(e.getTo()) > 10) {
                    if(!Duell.moveGetInt.containsKey(e.getPlayer())){
                        Duell.moveGetInt.put(mp, Duell.moveInt);
                        Duell.moveGetInt.put(e.getPlayer(), Duell.moveInt);
                        Duell.movePlayer.put(Duell.moveInt, new ArrayList<Player>());
                        Duell.movePlayer.get(Duell.moveInt).add(e.getPlayer());
                        Duell.movePlayer.get(Duell.moveInt).add(mp);
                        Duell.moveRemainingTime.put(Duell.moveInt, 5);
                        Duell.moveInt++;
                    }

                } else if (mp.getLocation().distance(e.getTo()) <= 10) {
                    if (Duell.moveGetInt.containsKey(e.getPlayer())) {
                        int moveId = Duell.moveGetInt.get(e.getPlayer());
                        Duell.moveRemainingTime.remove(moveId);
                        Duell.movePlayer.remove(moveId);
                        Duell.moveGetInt.remove(e.getPlayer());
                        Duell.moveGetInt.remove(mp);
                        Duell.matchShowTime.put(matchId, true);
                    }
                }
            }
        }
    }
}
