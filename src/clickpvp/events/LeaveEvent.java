package clickpvp.events;

import clickpvp.Duell;
import clickpvp.Duell.winType;
import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveEvent implements Listener {

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        if (Duell.matchGetInt.containsKey(e.getPlayer())) {
            int matchId = Duell.matchGetInt.get(e.getPlayer());
            ArrayList al = (ArrayList) Duell.matchPlayers.get(matchId).clone();
            al.remove(e.getPlayer());
            ((Player) al.iterator().next()).sendMessage(Duell.prefix + ChatColor.GREEN + "Du hast das Duell gegen " + ChatColor.GOLD + e.getPlayer().getName() + ChatColor.GREEN + " gewonnen, da " + ChatColor.GOLD + e.getPlayer().getName() + ChatColor.GREEN + " das Spiel verlassen hat.");
            Duell.endDuell((Player) al.iterator().next(), e.getPlayer(), matchId, winType.DISCONNECT);
        }
        if (Duell.delayGetInt.containsKey(e.getPlayer())) {
            int id = Duell.delayGetInt.get(e.getPlayer());
            Duell.delayGetInt.remove(Duell.delayTimerpPlayer.get(id));
            Duell.delayGetInt.remove(Duell.delayTimetpPlayer.get(id));
            Duell.delayTimerpPlayer.remove(id);
            Duell.delayTimetpPlayer.remove(id);
            Duell.delayTimeLong.remove(id);
        }
    }

}
